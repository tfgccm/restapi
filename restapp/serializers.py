from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.compat import PasswordField
from restapp.models import (
    TipoSensor,
    Temperatura,
    Humedad,
    Alarmas,
    Unidades,
    ModoFuncionamiento,
    Mensajes,
    TipoModo,
    IPPublica
)
from fcm_django.models import FCMDevice
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_framework.serializers import ValidationError
from rest_framework.validators import UniqueValidator


##### TIPO SENSOR #####

class TipoSensorSerializer(serializers.ModelSerializer):

    class Meta:
        model = TipoSensor
        fields = ('id', 'nombre', 'descripcion')


##### UNIDAD #####

class UnidadesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unidades
        fields = ('id', 'nombre', 'simbolo', 'tipo')


##### MENSAJES #####

class MensajesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mensajes
        fields = ('id', 'descripcion')


##### TIPO MODO DE FUNCIONAMIENTO #####

class TipoModoSerializer(serializers.ModelSerializer):

    class Meta:
        model = TipoModo
        fields = ('id', 'modo', 'descripcion')


##### MODO DE FUNCIONAMIENTO #####

class ModoFuncionamientoSerializer(serializers.ModelSerializer):

    modo = TipoModoSerializer(read_only=True)
    modo_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=TipoModo.objects.all(), source='modo')

    class Meta:
        model = ModoFuncionamiento
        fields = ('id', 'modo_id', 'modo')
        # Tan sólo permitimos que se actualice el "estado" del modo de funcionamiento.
        read_only_fields = ('id', 'modo')


##### TEMPERATURA #####

class TemperaturaSerializer(serializers.ModelSerializer):

    # Declaramos un campo de solo lectura que obtiene el objeto completo anidado,
    # y otro de sólo escritura en la que tan sólo indicaremos el id.
    unidad = UnidadesSerializer(read_only=True)
    unidad_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Unidades.objects.all(), source='unidad')

    class Meta:
        model = Temperatura
        fields = ('id', 'valor', 'unidad', 'unidad_id', 'fecha_lectura')


##### HUMEDAD #####

class HumedadSerializer(serializers.ModelSerializer):

    # Declaramos un campo de solo lectura que obtiene el objeto completo anidado,
    # y otro de sólo escritura en la que tan sólo indicaremos el id.
    unidad = UnidadesSerializer(read_only=True)
    unidad_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Unidades.objects.all(), source='unidad')

    class Meta:
        model = Humedad
        fields = ('id', 'valor', 'unidad', 'unidad_id', 'fecha_lectura')


##### ALARMA #####

class AlarmaSerializer(serializers.ModelSerializer):

    # Declaramos un campo de solo lectura que obtiene el objeto completo anidado,
    # y otro de sólo escritura en la que tan sólo indicaremos el id.
    tipo = TipoSensorSerializer(read_only=True)
    tipo_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=TipoSensor.objects.all(), source='tipo')
    mensaje = MensajesSerializer(read_only=True)
    mensaje_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Mensajes.objects.all(), source='mensaje')

    class Meta:
        model = Alarmas
        fields = ('id', 'tipo_id', 'tipo', 'mensaje_id', 'mensaje', 'fecha_alarma')


##### IP PÚBLICA DE RASPBERRY PI #####

class IPPublicaSerializer(serializers.ModelSerializer):

    class Meta:
        model = IPPublica
        fields = ('id', 'ip')


User = get_user_model()


##### REGISTRAR #####

class RegistrarSerializer(serializers.ModelSerializer):

    username = serializers.CharField(max_length=50,
                                     # Validación de campo único
                                     validators=[UniqueValidator(queryset=User.objects.all(), message='El Usuario introducido ya está en uso')])
    email = serializers.EmailField(required=True,
                                   # Validación de campo único
                                   validators=[UniqueValidator(queryset=User.objects.all(), message='El Email introducido ya está en uso')])
    password = serializers.CharField(max_length=50, write_only=True, trim_whitespace=False)
    password2 = serializers.CharField(max_length=50, write_only=True, trim_whitespace=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')

    # Validación del campo 'password'
    def validate_password(self, value):
        # Se obtiene un OrderedDict con los valores introducidos en el formulario
        data = self.get_initial()
        # Obtenemos el valor de la contraseña y confirmación de contraseña
        password2 = data.get('password2')
        password = value
        # Si su valor es distinto lanzar, ejecutar error de validación
        if password != password2:
            raise ValidationError('Las Contraseñas deben coincidir')
        return value

    # Validación del campo 'passowrd2', proceso equivalente a la validación de 'password'
    def validate_password2(self, value):
        # Se obtiene un OrderedDict con los valores introducidos en el formulario
        data = self.get_initial()
        password = data.get('password')
        password2 = value
        if password2 != password:
            raise ValidationError('Las Contraseñas deben coincidir')
        return value

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        # Creamos el obtjeto usuario:
        usr_obj = User(
            username=username,
            email=email,
        )
        # Empleamos set_password para encriptar la contraseña introducida por el usuario
        usr_obj.set_password(password)
        usr_obj.save()
        # Asignamos al usuario el rol 'clienteAndroid'
        usr_grp = Group.objects.get(name='clienteAndroid')
        usr_obj.groups.add(usr_grp)
        return validated_data


#### ACCEDER #####

class AccederSerializer(JSONWebTokenSerializer):

    username = serializers.CharField(max_length=50)
    password = PasswordField(max_length=50, write_only=True)


##### DISPOSITIVOS FCM #####

class DispositivosSerializer(serializers.ModelSerializer):

    class Meta:
        model = FCMDevice
        fields = (
            "id", "name", "registration_id", "device_id", "active",
            "date_created", "type"
        )

        read_only_fields = ("date_created",)
        extra_kwargs = {"active": {"default": True}}

