
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from restapp.permissions import UsuarioR_AdminCRUD, UsuarioRU_AdminCRUD, UsuarioC_AdminC, UsuarioD_AdminD
from rest_framework.settings import api_settings
from restapp.models import TipoSensor, Temperatura, Humedad, Alarmas, Unidades, ModoFuncionamiento, TipoModo, IPPublica, Mensajes
from django.contrib.auth import get_user_model
from restapp import services
from restapp.serializers import (
    TemperaturaSerializer,
    HumedadSerializer,
    AlarmaSerializer,
    ModoFuncionamientoSerializer,
    IPPublicaSerializer,
    TipoModoSerializer,
    TipoSensorSerializer,
    UnidadesSerializer,
    RegistrarSerializer,
    AccederSerializer,
    DispositivosSerializer,
    MensajesSerializer
)
from rest_framework.generics import (

    ListAPIView,
    ListCreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateDestroyAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
)
from rest_framework_jwt.views import ObtainJSONWebToken
from fcm_django.models import FCMDevice
from fcm_django.api.rest_framework import FCMDeviceSerializer
from fcm_django.api.rest_framework import FCMDeviceViewSet, FCMDeviceAuthorizedViewSet

from rest_framework_swagger.views import get_swagger_view


# Esquema de vistas Swagger
schema_view = get_swagger_view(title='SIDOCS REST API')


User = get_user_model()

# Create your views here.

##### TIPO SENSOR #####

# Visualización de listado

class TipoSensorList(ListAPIView):
    queryset = TipoSensor.objects.all()
    serializer_class = TipoSensorSerializer


# Visualización de elemento

class TipoSensorDetail(RetrieveAPIView):
    queryset = TipoSensor.objects.all()
    serializer_class = TipoSensorSerializer

##### UNIDAD #####


# Visualización de listado

class UnidadList(ListAPIView):
    queryset = Unidades.objects.all()
    serializer_class = UnidadesSerializer


# Visualización de elemento

class UnidadDetail(RetrieveAPIView):
    queryset = Unidades.objects.all()
    serializer_class = UnidadesSerializer

##### MENSAJES #####


# Visualización de listado

class MensajesList(ListAPIView):
    queryset = Mensajes.objects.all()
    serializer_class = MensajesSerializer


# Visualización de elemento

class MensajesDetail(RetrieveAPIView):
    queryset = Mensajes.objects.all()
    serializer_class = MensajesSerializer

##### TIPO MODO DE FUNCIONAMIENTO #####


# Visualización de listado

class TipoModoList(ListAPIView):
    queryset = TipoModo.objects.all()
    serializer_class = TipoModoSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización de listado

class TipoModoDetail(RetrieveAPIView):
    queryset = TipoModo.objects.all()
    serializer_class = TipoModoSerializer
    permission_classes = [UsuarioR_AdminCRUD]


##### MODO DE FUNCIONAMIENTO #####

# Visualización de listado

class ModoFuncionamientoList(ListAPIView):
    queryset = ModoFuncionamiento.objects.all()
    serializer_class = ModoFuncionamientoSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


# Visualización del último elemento insertado en la tabla

class ModoFuncionamientoDetailLast(ListAPIView):
    queryset = ModoFuncionamiento.objects.order_by('-pk')[:1]
    serializer_class = ModoFuncionamientoSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


# Visualización/Actualización de elemento

class ModoFuncionamientoDetail(RetrieveUpdateAPIView):
    queryset = ModoFuncionamiento.objects.all()
    serializer_class = ModoFuncionamientoSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


##### TEMPERATURA #####

# Visualización de listado y creación de elemento

class TemperaturaList(ListCreateAPIView):
    queryset = Temperatura.objects.all()
    serializer_class = TemperaturaSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización del último elemento insertado en la tabla

class TemperaturaDetailLast(ListAPIView):
    queryset = Temperatura.objects.order_by('-fecha_lectura')[:1]
    serializer_class = TemperaturaSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización de elemento

class TemperaturaDetail(RetrieveAPIView):
    queryset = Temperatura.objects.all()
    serializer_class = TemperaturaSerializer
    permission_classes = [UsuarioR_AdminCRUD]


##### HUMEDAD #####

# Visualización de listado y creación de elemento

class HumedadList(ListCreateAPIView):
    queryset = Humedad.objects.all()
    serializer_class = HumedadSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización del último elemento insertado en la tabla

class HumedadDetailLast(ListAPIView):
    queryset = Humedad.objects.order_by('-fecha_lectura')[:1]
    serializer_class = HumedadSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización de elemento

class HumedadDetail(RetrieveAPIView):
    queryset = Humedad.objects.all()
    serializer_class = HumedadSerializer
    permission_classes = [UsuarioR_AdminCRUD]


##### ALARMA #####


# Visualización de listado y creación de elemento

class AlarmaList(ListCreateAPIView):
    queryset = Alarmas.objects.all()
    serializer_class = AlarmaSerializer
    permission_classes = [UsuarioR_AdminCRUD]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        # Llamamos al servicio de envío de mail y notificaciones FCM pasando por parámetro el IdTipo de la alarma
        idTipo = serializer.validated_data['tipo'].id
        services.send_email(idTipo)
        services.send_notification(idTipo)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


# Visualización de los últimos 10 elementos insertados en la tabla

class AlarmaDetailLast(ListAPIView):
    queryset = Alarmas.objects.all().order_by('-fecha_alarma')[:10]
    serializer_class = AlarmaSerializer
    permission_classes = [UsuarioR_AdminCRUD]


# Visualización/Actualización/Eliminación de elemento

class AlarmaDetail(RetrieveUpdateDestroyAPIView):
    queryset = Alarmas.objects.all()
    serializer_class = AlarmaSerializer
    permission_classes = [UsuarioR_AdminCRUD]


##### IP PÚBLICA DE RASPBERRY PI #####

# Visualización de listado

class IPPublicaList(ListAPIView):
    queryset = IPPublica.objects.all()
    serializer_class = IPPublicaSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


# Visualización del último elemento insertado en la tabla

class IPPublicaDetailLast(ListAPIView):
    queryset = IPPublica.objects.order_by('-pk')[:1]
    serializer_class = IPPublicaSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


# Visualización/Actualización de elemento

class IPPublicaDetail(RetrieveUpdateAPIView):
    queryset = IPPublica.objects.all()
    serializer_class = IPPublicaSerializer
    permission_classes = [UsuarioRU_AdminCRUD]


##### REGISTRAR #####


class RegistrarAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegistrarSerializer
    permission_classes = [AllowAny]


#### ACCEDER #####


class AccederAPIView(ObtainJSONWebToken):
    serializer_class = AccederSerializer
    permission_classes = [AllowAny]


##### DISPOSITIVOS FCM #####


class FCMDeviceViewSetCustom(FCMDeviceViewSet):
    queryset = FCMDevice.objects.all()
    serializer_class = FCMDeviceSerializer
    permission_classes = [AllowAny]


# Creación de dispositivo

class DispositivosList(FCMDeviceAuthorizedViewSet):
    queryset = FCMDevice.objects.all()
    serializer_class = FCMDeviceSerializer
    permission_classes = [UsuarioC_AdminC]

# Eliminación de dispositivos

class DispositivosDetail(DestroyAPIView):
    queryset = FCMDevice.objects.all()
    serializer_class = DispositivosSerializer(many=True)
    lookup_url_kwarg = 'registration_id'
    permission_classes = [UsuarioD_AdminD]

    # Sobreescribimos el método get_queryset para obtener el listado de dispositivos con registration_id = parámetro URL
    def get_queryset(self):
        registration_id = self.kwargs.get(self.lookup_url_kwarg)
        dispositivos = FCMDevice.objects.filter(registration_id=registration_id)
        return dispositivos

    def destroy(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        self.perform_destroy(queryset)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()
