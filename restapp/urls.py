from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import refresh_jwt_token
from restapp import views
from django.conf.urls import include
from django.conf.urls import url

urlpatterns = [

# DOCUMENTACIÓN DE LA API
url(r'^documentacion/', views.schema_view),
# Añade los procesos de acceso y cierre de sesión a la API navegable, usado por Swagger
url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

# REGISTRAR USUARIO ANDROID
url(r'^registrar/$', views.RegistrarAPIView.as_view()),

# OBTENER TOKEN DE ACCESO
url(r'^acceder/$', views.AccederAPIView.as_view()),

# REFRESCAR TOKEN DE ACCESO
url(r'^refrescartoken/$', refresh_jwt_token),

# LISTAR TIPOS DE SENSOR, OBTENER TIPO DE SENSOR SOLICITADO (PK)
url(r'^tiposensorlist/$', views.TipoSensorList.as_view()),
url(r'^tiposensor/(?P<pk>[0-9]+)$', views.TipoSensorDetail.as_view()),

# LISTAR UNIDADES DE MEDIDA, OBTENER UNIDAD DE MEDIDA SOLICITADA (PK)
url(r'^unidadlist/$', views.UnidadList.as_view()),
url(r'^unidad/(?P<pk>[0-9]+)$', views.UnidadDetail.as_view()),

# LISTAR MENSAJES, OBTENER MENSAJE SOLICITADO (PK)
url(r'^mensajeslist/$', views.MensajesList.as_view()),
url(r'^mensajes/(?P<pk>[0-9]+)$', views.MensajesDetail.as_view()),

# LISTAR TEMPERATURA, OBTENER TEMPERATURA SOLICITADA (PK), OBTENER ÚLTIMA TEMPERATURA REGISTRADA
url(r'^temperaturalist/$', views.TemperaturaList.as_view()),
url(r'^temperatura/(?P<pk>[0-9]+)$', views.TemperaturaDetail.as_view()),
url(r'^temperaturalast/$', views.TemperaturaDetailLast.as_view()),

# LISTAR HUMEDAD, OBTENER HUMEDAD SOLICITADA (PK), OBTENER ÚLTIMA HUMEDAD REGISTRADA
url(r'^humedadlist/$', views.HumedadList.as_view()),
url(r'^humedad/(?P<pk>[0-9]+)$', views.HumedadDetail.as_view()),
url(r'^humedadlast/$', views.HumedadDetailLast.as_view()),

# LISTAR ALARMAS, OBTENER ALARMA SOLICITADA (PK), OBTENER 10 ÚLTIMAS ALARMAS
url(r'^alarmalist/$', views.AlarmaList.as_view()),
url(r'^alarma/(?P<pk>[0-9]+)$', views.AlarmaDetail.as_view()),
url(r'^alarmalast/$', views.AlarmaDetailLast.as_view()),

# LISTAR TIPOS DE MODO DE FUNCIONAMIENTO, OBTENER MODO DE FUNCIONAMIENTO SOLICITADO (PK)
url(r'^tipomodolist/$', views.TipoModoList.as_view()),
url(r'^tipomodo/(?P<pk>[0-9]+)$', views.TipoModoDetail.as_view()),

# OBTENER MODOS HISTÓRICOS, OBTENER MODO SOLICITADO (PK), OBTENER ÚLTIMO MODO REGISTRADO
url(r'^modolist/$', views.ModoFuncionamientoList.as_view()),
url(r'^modo/(?P<pk>[0-9]+)$', views.ModoFuncionamientoDetail.as_view()),
url(r'^modolast/$', views.ModoFuncionamientoDetailLast.as_view()),

# OBTENER IPs HISTÓRICAS, OBTENER IP SOLICITADA (PK), OBTENER ÚLTIMA IP REGISTRADA
url(r'^iplist/$', views.IPPublicaList.as_view()),
url(r'^ip/(?P<pk>[0-9]+)$', views.IPPublicaDetail.as_view()),
url(r'^iplast/$', views.IPPublicaDetailLast.as_view()),

# REGISTRAR DISPOSITIVO PARA EL USUARIO SOLICITANTE, ELIMINAR DISPOSITIVOS PARA registration_id DADO
url(r'^dispositivos/registrar/?$', views.DispositivosList.as_view({'post': 'create'}), name='registrar_dispositivo_fcm'),
url(r'^dispositivos/eliminar/(?P<registration_id>.*)$', views.DispositivosDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)