from django.db import models
# Create your models here.

# MODELOS DE INTEGRACIÓN CON BBDD

##### TIPO SENSOR #####

class TipoSensor(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=150)

    def __str__(self):
        return str(self.descripcion)

    class Meta:
        ordering = ('nombre',)

##### UNIDAD #####

class Unidades(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    simbolo = models.CharField(max_length=5)
    tipo = models.ForeignKey(TipoSensor, on_delete=models.CASCADE, )

    def __str__(self):
        return str(self.simbolo)

    class Meta:
        ordering = ('nombre',)


##### MENSAJES (EMAIL & PUSH) #####

class Mensajes(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=300)

    def __str__(self):
        return str(self.descripcion)

    class Meta:
        ordering = ('id',)


##### TIPO MODO DE FUNCIONAMIENTO #####

class TipoModo(models.Model):
    id = models.IntegerField(primary_key=True)
    modo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=300)

    def __str__(self):
        return str(self.modo)

    class Meta:
        ordering = ('id',)


##### MODO DE FUNCIONAMIENTO #####

class ModoFuncionamiento(models.Model):
    id = models.IntegerField(primary_key=True)
    modo = models.ForeignKey(TipoModo, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return str(self.modo)

    class Meta:
        ordering = ('id',)


##### TEMPERATURA #####

class Temperatura(models.Model):
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    unidad = models.ForeignKey(Unidades, on_delete=models.CASCADE,)
    fecha_lectura = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('fecha_lectura',)


##### HUMEDAD #####

class Humedad(models.Model):
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    unidad = models.ForeignKey(Unidades, on_delete=models.CASCADE, )
    fecha_lectura = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('fecha_lectura',)


##### ALARMA #####

class Alarmas(models.Model):
    tipo = models.ForeignKey(TipoSensor, on_delete=models.CASCADE)
    mensaje = models.ForeignKey(Mensajes, on_delete=models.CASCADE, default=1)
    fecha_alarma = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('fecha_alarma',)


##### IP PÚBLICA DE RASPBERRY PI #####

class IPPublica(models.Model):
    id = models.IntegerField(primary_key=True)
    ip = models.CharField(max_length=15)

    def __str__(self):
        return str(self.ip)

    class Meta:
        ordering = ('id',)