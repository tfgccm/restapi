from restapi import mail_config
from django.core.mail import send_mass_mail, send_mail, EmailMultiAlternatives
from restapi import settings
from django.contrib.auth.models import User
from restapp.models import Mensajes
from fcm_django.models import FCMDevice
from django.template import loader

#CAPA DE SERVICIOS DE COMUNICACIÓN (EMAIL, FCM)

def send_email(tipo):

    #Evualuamos el tipo de la alarma insertada en BBDD y elegimos el mensaje de correo a enviar

    if tipo == 3:       # Sensor de Agua
        message_id = 5

    elif tipo == 4:     # Sensor de Gas
        message_id = 4

    elif tipo == 5:     # Sensor PIR
        message_id = 6

    else:
        return

    #Obtenemos los parámetros del emil

    #Emisor
    from_email = settings.EMAIL_HOST_USER

    #Listado de receptores
    receipt_list = User.objects.values_list('email', flat=True)

    #Asunto
    subject = mail_config.SUBJECT_ALERT

    #Obtenemos el mensaje a enviar
    mensaje = Mensajes.objects.values_list('descripcion', flat=True).get(pk=message_id)
    #Cuerpo del mensaje con parámetro de entrada 'message'.
    html_message = loader.render_to_string('email_alarma.html', {'mensaje_alarma': mensaje})

    #Definimos el objeto mensaje a enviar
    message = EmailMultiAlternatives(subject=subject, from_email=from_email, bcc=receipt_list)
    #Adjuntamos la plantilla HTML
    message.attach_alternative(html_message, "text/html")
    #Envío del mensaje
    message.send()


def send_notification(tipo):

    #Evualuamos el tipo de la alarma insertada en BBDD y elegimos el mensaje de notificación push a enviar

    if tipo == 3:       # Sensor de Agua
        message_id = 2

    elif tipo == 4:     # Sensor de Gas
        message_id = 1

    elif tipo == 5:     # Sensor PIR
        message_id = 3

    else:
        return

    #Obtenemos el mensaje a enviar
    message = Mensajes.objects.values_list('descripcion',flat=True).get(pk=message_id)
    #Los dispositivos destinatarios serán todos aquellos activos
    devices = FCMDevice.objects.all().filter(active=True)
    #Enviamos el mensaje de notificación push
    devices.send_data_message(data_message={"body": message, "title": "Alerta SIDOCS"})




