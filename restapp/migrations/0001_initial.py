# Generated by Django 2.1.dev20180405145536 on 2018-09-14 17:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alarmas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_alarma', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('fecha_alarma',),
            },
        ),
        migrations.CreateModel(
            name='Humedad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('fecha_lectura', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('fecha_lectura',),
            },
        ),
        migrations.CreateModel(
            name='Mensajes',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=300)),
            ],
            options={
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='ModoFuncionamiento',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
            ],
            options={
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='Temperatura',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('fecha_lectura', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('fecha_lectura',),
            },
        ),
        migrations.CreateModel(
            name='TipoModo',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('modo', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=300)),
            ],
            options={
                'ordering': ('id',),
            },
        ),
        migrations.CreateModel(
            name='TipoSensor',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=150)),
            ],
            options={
                'ordering': ('nombre',),
            },
        ),
        migrations.CreateModel(
            name='Unidades',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
                ('simbolo', models.CharField(max_length=5)),
                ('tipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restapp.TipoSensor')),
            ],
            options={
                'ordering': ('nombre',),
            },
        ),
        migrations.AddField(
            model_name='temperatura',
            name='unidad',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restapp.Unidades'),
        ),
        migrations.AddField(
            model_name='modofuncionamiento',
            name='modo',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='restapp.TipoModo'),
        ),
        migrations.AddField(
            model_name='humedad',
            name='unidad',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restapp.Unidades'),
        ),
        migrations.AddField(
            model_name='alarmas',
            name='mensaje',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='restapp.Mensajes'),
        ),
        migrations.AddField(
            model_name='alarmas',
            name='tipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restapp.TipoSensor'),
        ),
    ]
