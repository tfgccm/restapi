from rest_framework.permissions import BasePermission

# PERMISOS DE LA APLICACIÓN

#C = CREATE (Crear), R = READ (Leer), U = UPDATE (Actualizar), D = DELETE (Borrar)
#C = POST , R = GET, U = PUT, D = DELETE

R_METHODS = ('GET', 'HEAD', 'OPTIONS')
RU_METHODS = ('GET', 'PUT', 'HEAD', 'OPTIONS')
CD_METHODS = ('CREATE', 'DELETE')
CU_METHODS = ('POST', 'PUT')
D_METHODS = ('DELETE',)


# USUARIOS LECTURA - ADMINISTRADOR LECTURA, ESCRITURA, ACTUALIZACIÓN Y BORRADO

class UsuarioR_AdminCRUD(BasePermission):

    def has_permission(self, request, view):

        # Peticiones 'GET', 'OPTIONS' y 'HEAD'
        if request.method in R_METHODS:
            # Restringimos la consulta de datos a usuarios autenticados.
            if request.user and request.user.is_authenticated:
                return True
            else:
                return False

        # Peticiones 'POST', 'UPDATE' y 'DELETE'
        else:
            # Restringimos las operaciones CRU a Superusuarios o al cliente usado por Raspberry Pi.
            if request.user.is_superuser or (request.user and request.user.groups.filter(name='clienteRP')):
                return True
            else:
                return False

# USUARIOS LECTURA Y ACTUALIZACIÓN - ADMINISTRADOR LECTURA, ESCRITURA, ACTUALIZACIÓN Y BORRADO

class UsuarioRU_AdminCRUD(BasePermission):

    def has_permission(self, request, view):

        # Peticiones 'GET', 'UPDATE', 'OPTIONS' y 'HEAD'
        if request.method in RU_METHODS:
            # Restringimos la consulta de datos a usuarios autenticados.
            if request.user and request.user.is_authenticated:
                return True
            else:
                return False


        else:
            # Restringimos las operaciones CRU a Superusuarios o al cliente usado por Raspberry Pi
            if request.user.is_superuser or (request.user and request.user.groups.filter(name='clienteRP')):
                return True
            else:
                return False

# USUARIOS ESCRITURA - ADMINISTRADOR ESCRITURA

class UsuarioC_AdminC(BasePermission):

    def has_permission(self, request, view):

        # Peticiones 'POST', 'UPDATE'
        if request.method in CU_METHODS:
            if (request.user and request.user.groups.filter(name='clienteAndroid')) or (request.user and request.user.groups.filter(name='clienteRP')):
                return True
            else:
                return False
        else:
            return False


# USUARIOS BORRADO - ADMINISTRADOR BORRADO

class UsuarioD_AdminD(BasePermission):

    def has_permission(self, request, view):

        # Peticiones 'DELETE'
        if request.method in D_METHODS:
            if (request.user and request.user.groups.filter(name='clienteAndroid')) or (request.user and request.user.groups.filter(name='clienteRP')):
                return True
            else:
                return False
        else:
            return False