"""
Django settings for restapi project.

Generated by 'django-admin startproject' using Django 1.11b1.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from restapi import mail_config
import datetime
import os

EMAIL_USE_TLS = mail_config.EMAIL_USE_TLS
EMAIL_HOST = mail_config.EMAIL_HOST
EMAIL_HOST_USER = mail_config.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = mail_config.EMAIL_HOST_PASSWORD
EMAIL_PORT = mail_config.EMAIL_PORT

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n&80usq*no*9!yc3^)xb#94-8-4^7+@u7z5&z8_%&qad%1ieps'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [u'ccmanzanero.pythonanywhere.com', 'www.ccmanzanero.pythonanywhere.com', 'localhost', '192.168.0.160']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Aplicación DRF
    'rest_framework',
    # Aplicación REST API
	'restapp',
    # Aplicación de integración con JWT
    'rest_framework_jwt',
    # Aplicación de integración con FCM
    'fcm_django',
    # Aplicación de integración con Swagger
    'rest_framework_swagger'
]

MIDDLEWARE = [

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # Permite establecer el idioma según la localización del cliente. Se define despues de SessionMiddleware ya que hace uso de ella
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]



ROOT_URLCONF = 'restapi.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'restapi.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases


##Comentar en local

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ccmanzanero$sidocsbbdd',
        'USER': 'ccmanzanero',
        'PASSWORD': 'carlos21',
        'HOST': 'ccmanzanero.mysql.pythonanywhere-services.com',
        'PORT': '',

        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    }
}


##Descomentar en local
'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'sidocsdb2',
        'USER': 'root',
        'PASSWORD': 'carlos21',
        'HOST': 'localhost',
        'PORT': '',

        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        }
    }
}
'''

# Password validation
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

# Idioma por defecto en caso de no detectarlo automáticamente por LocaleMiddleware
LANGUAGE_CODE = 'es-es'

TIME_ZONE = 'Europe/Madrid'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'

# default static files settings for PythonAnywhere.
# see https://help.pythonanywhere.com/pages/DjangoStaticFiles for more info

MEDIA_ROOT = u'/home/ccmanzanero/restapi/media'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
#STATIC_ROOT = u'/home/ccmanzanero/restapi/static'
STATIC_URL = '/static/'

'''STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    u'/home/ccmanzanero/.virtualenvs/sidocs/lib/python3.6/site-packages/rest_framework_swagger/static',
]'''



#Cambiar en local a False
SECURE_SSL_REDIRECT = True
#SECURE_SSL_REDIRECT = False


# Configuración DRF
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        # Por defecto el método de autenticación será JWT
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        # Añadimos la autenticación por sesión, para habilitar la navegación de la interfaz web Swagger
        'rest_framework.authentication.SessionAuthentication'
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        # Por defecto definimos el nivel de permisos en "Autenticado"
        'rest_framework.permissions.IsAuthenticated',
    ),

    # Formato de fechas
    'DATETIME_FORMAT': "%d-%m-%Y - %H:%M:%S",
}


# Configuración JWT
JWT_AUTH = {
    # Tiempo máximo para expiración de token (seconds=900, 15 min)
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=900),
    'JWT_ALLOW_REFRESH': True,
    # Tiempo máximo para refresco de token. Transcurrido, se requerirá una nueva autenticación (days=7)
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=1),
}

# Configuración FCM
FCM_DJANGO_SETTINGS = {
    "FCM_SERVER_KEY": 'AAAAfKAr2-U:APA91bGzHc4Ssybbn3FREVNq9KD1hAobRaIv0_56Bj0JCkNn16TIOTzAkjxd2OueMcDqwC-AYJfHbAZ-0HjJzfjNj6c0I3PczkEbITkd-EE1OzVs3P6bgiWdpv44T5FCPF4QfxKHHXV7',
    # No se permitirá asignar un dispositivo a dos usuarios android activos
    "ONE_DEVICE_PER_USER": True,
    "DELETE_INACTIVE_DEVICES": True,
}

# Configuración SWAGGER
SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
    'LOGIN_URL': 'rest_framework:login',
    'LOGOUT_URL': 'rest_framework:logout',
}
