
from django.conf.urls import url, include
from django.contrib import admin

#CONFIGURACIÓN DE URLS DE RESTAPI

urlpatterns = [
    #Sitio de administración de la API
    url(r'^admin/', admin.site.urls),
    #Raíz de la API
    url(r'^api/', include('restapp.urls')),
]
